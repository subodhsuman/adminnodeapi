import  Sequelize from  'sequelize';

const database = new Sequelize('database', 'username', 'password', {
    host: 'localhost',
    dialect:  'mysql',
    logging:false, 
  });


  try {
    await database.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }